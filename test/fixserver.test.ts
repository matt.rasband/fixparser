import FIXServer, { EncryptMethod, Field, Fields, Message, Messages } from '../src/FIXServer';
import FIXParser from '../src/FIXParser';

describe('FIXServer', () => {
    let fixServer: FIXServer = new FIXServer();
    let fixParser: FIXParser = new FIXParser();
    describe('TCP', () => {
        it('End-to-end: connect', (done) => {
            fixServer = new FIXServer();
            fixParser = new FIXParser();
            const HOST: string = 'localhost';
            const PORT: number = 9801;

            // Start up a server
            fixServer.createServer({ host: HOST, port: PORT, protocol: 'tcp', logging: false });
            fixServer.once('message', (message) => {
                expect(['Logon', 'Heartbeat']).toContain(message.description);
                fixParser.close();
                fixServer.close();
                done();
            });

            // Connect with a client
            fixParser.connect({
                host: HOST,
                port: PORT,
                protocol: 'tcp',
                sender: 'sender',
                target: 'target',
                fixVersion: 'FIX.5.0',
                logging: false,
            });
            fixParser.on('open', () => {
                // Send a Logon message
                const logon: Message = fixParser.createMessage(
                    new Field(Fields.MsgType, Messages.Logon),
                    new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
                    new Field(Fields.SenderCompID, fixParser.sender),
                    new Field(Fields.SendingTime, fixParser.getTimestamp()),
                    new Field(Fields.TargetCompID, fixParser.target),
                    new Field(Fields.ResetSeqNumFlag, 'Y'),
                    new Field(Fields.EncryptMethod, EncryptMethod.None),
                    new Field(Fields.HeartBtInt, 60),
                );
                fixParser.send(logon);
            });
            fixParser.on('error', () => {});
        });
    });

    describe('websocket', () => {
        fit('End-to-end: connect', (done) => {
            fixServer = new FIXServer();
            fixParser = new FIXParser();
            const HOST: string = 'localhost';
            const PORT: number = 9802;

            // Start up a server
            fixServer.createServer({ host: HOST, port: PORT, protocol: 'websocket', logging: false });
            fixServer.once('message', (message) => {
                expect(['Logon', 'Heartbeat']).toContain(message.description);
                fixParser.close();
                fixServer.close();
                done();
            });

            // Connect with a client
            fixParser.connect({
                host: HOST,
                port: PORT,
                protocol: 'websocket',
                sender: 'sender',
                target: 'target',
                fixVersion: 'FIX.5.0',
                logging: false,
            });
            fixParser.on('open', () => {
                // Send a Logon message
                const logon: Message = fixParser.createMessage(
                    new Field(Fields.MsgType, Messages.Logon),
                    new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
                    new Field(Fields.SenderCompID, fixParser.sender),
                    new Field(Fields.SendingTime, fixParser.getTimestamp()),
                    new Field(Fields.TargetCompID, fixParser.target),
                    new Field(Fields.ResetSeqNumFlag, 'Y'),
                    new Field(Fields.EncryptMethod, EncryptMethod.None),
                    new Field(Fields.HeartBtInt, 60),
                );
                fixParser.send(logon);
            });
        });
    });
});
