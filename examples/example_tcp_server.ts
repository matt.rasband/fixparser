import FIXServer from '../src/FIXServer'; // from 'fixparser/server';

const fixServer = new FIXServer();
fixServer.createServer({
    host: 'localhost',
    port: 9878,
    sender: 'SERVER',
    target: 'CLIENT',
});

fixServer.on('open', () => {
    console.log('Open');
});
fixServer.on('message', (message) => {
    console.log('server received message', message.description, message.messageString);
});
fixServer.on('close', () => {
    console.log('Disconnected');
});
