/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2021 Victor Norgren
 * Released under the MIT license
 */
import { EventEmitter } from 'events';
import { Server, createServer as createNetServer, Socket } from 'net';
import Websocket from 'ws';

import * as Constants from './fieldtypes';
import { Field } from './fields/Field';
import FIXParser, { Protocol } from './FIXParser';
import { Message } from './message/Message';
import { version, DEFAULT_FIX_VERSION, log, logError, Version, loggingSettings } from './util/util';
import { FrameDecoder } from './util/FrameDecoder';
import { MessageEnum } from './fieldtypes/MessageEnum';
import { Options as FIXParserOptions } from './FIXParser';
import { MessageBuffer } from './util/MessageBuffer';
import { heartBeat } from './messagetemplates/MessageTemplates';
import { handleLogon } from './session/SessionLogon';
import { handleLogout } from './session/SessionLogout';
import { handleTestRequest } from './session/SessionTestRequest';
import { handleSequenceReset } from './session/SessionSequenceReset';
import { handleSequence } from './session/SessionSequence';

type Options = Pick<
    FIXParserOptions,
    'host' | 'port' | 'protocol' | 'sender' | 'target' | 'heartbeatIntervalSeconds' | 'fixVersion' | 'logging'
>;

export default class FIXServer extends EventEmitter {
    public static version: Version = version;

    public fixParser: FIXParser = new FIXParser();
    public host: string = 'localhost';
    public port: number = 9878;
    public protocol: Protocol = 'tcp';
    public server: Server | Websocket.Server | null = null;
    public sender: string = '';
    public target: string = '';
    public heartBeatInterval: number | undefined;
    public fixVersion: string = DEFAULT_FIX_VERSION;
    public nextNumIn: number = 1;

    private heartBeatIntervalId: ReturnType<typeof setInterval> | null = null;
    private socket: WebSocket | Socket | null = null;
    private connected: boolean = false;
    private messageBuffer: MessageBuffer = new MessageBuffer();

    public createServer({
        host = this.host,
        port = this.port,
        protocol = this.protocol,
        sender = this.sender,
        target = this.target,
        heartbeatIntervalSeconds = 30,
        fixVersion = this.fixVersion,
        logging = true,
    }: Options = {}): void {
        this.host = host;
        this.port = port;
        this.protocol = protocol;
        this.sender = sender;
        this.target = target;
        this.fixParser.sender = sender;
        this.fixParser.target = target;
        this.heartBeatInterval = heartbeatIntervalSeconds;
        this.fixVersion = fixVersion;
        loggingSettings.enabled = logging;
        this.initialize();
    }

    private initialize() {
        if (this.protocol === 'tcp') {
            this.server = createNetServer((socket: Socket) => {
                this.socket = socket;
                this.socket.pipe(new FrameDecoder()).on('data', (data: any) => {
                    const messages = this.parse(data.toString());
                    let i = 0;
                    for (i; i < messages.length; i++) {
                        this.processMessage(messages[i]);
                        this.emit('message', messages[i]);
                    }
                });
                this.socket.on('connect', () => {
                    log('FIXServer (TCP): connection established');
                    this.connected = true;
                    this.emit('open');
                });
                this.socket.on('close', () => {
                    this.connected = false;
                    this.stopHeartbeat();
                    this.resetSession();
                    log('FIXServer (TCP): closed connection');
                    this.emit('close');
                });
                this.socket.on('timeout', () => {
                    this.connected = false;
                    this.stopHeartbeat();
                    this.close();
                    this.resetSession();
                    logError('FIXServer (TCP): connection timeout');
                    this.emit('close');
                });
                this.socket.on('error', (error: Error) => {
                    this.connected = false;
                    this.stopHeartbeat();
                    this.close();
                    this.resetSession();
                    logError('FIXServer (TCP): error', error);
                    this.emit('error', error);
                });
            });
            this.server.listen(this.port, this.host, () => {
                log(`FIXServer (TCP) listening for connections at ${this.host}:${this.port}`);
            });
        } else if (this.protocol === 'websocket') {
            this.server = new Websocket.Server({ host: this.host, port: this.port });
            this.server.on('connection', (socket) => {
                this.connected = true;
                socket.on('message', (data: string | Buffer) => {
                    const messages = this.parse(data.toString());
                    let i = 0;
                    for (i; i < messages.length; i++) {
                        this.processMessage(messages[i]);
                        this.emit('message', messages[i]);
                    }
                });
            });
            this.server.on('close', () => {
                this.connected = false;
                this.stopHeartbeat();
                this.close();
                this.emit('close');
                log('FIXServer (WEBSOCKET): closed connection');
            });
            this.server.on('error', () => {
                this.connected = false;
                this.stopHeartbeat();
                this.close();
                this.emit('error');
                logError('FIXServer (WEBSOCKET): error');
            });
            this.server.on('listening', () => {
                log(`FIXServer (WEBSOCKET) listening for connections at ${this.host}:${this.port}`);
            });
        } else {
            logError('FIXServer: create server, invalid protocol');
        }
    }

    public getNextTargetMsgSeqNum(): number {
        return this.fixParser.getNextTargetMsgSeqNum();
    }

    public setNextTargetMsgSeqNum(nextMsgSeqNum: number): number {
        return this.fixParser.setNextTargetMsgSeqNum(nextMsgSeqNum);
    }

    public getTimestamp(dateObject = new Date()): string {
        return this.fixParser.getTimestamp(dateObject);
    }

    public createMessage(...fields: Field[]): Message {
        return this.fixParser.createMessage(...fields);
    }

    public parse(data: string): Message[] {
        return this.fixParser.parse(data);
    }

    public send(message: Message): void {
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket! as Socket;
            const encodedMessage: string = message.encode();
            this.fixParser.setNextTargetMsgSeqNum(this.fixParser.getNextTargetMsgSeqNum() + 1);
            if (!socket.write(encodedMessage)) {
                logError('FIXServer (TCP): could not send message, socket not open');
            } else {
                this.messageBuffer.add(message.clone());
                log('FIXServer (TCP): >> sent', encodedMessage.replace(/\x01/g, '|'));
            }
        } else if (this.protocol === 'websocket') {
            const server: Websocket.Server = this.server! as Websocket.Server;
            const encodedMessage: string = message.encode();
            if (server && server.clients && server.clients.size > 0) {
                server.clients.forEach((client: Websocket) => {
                    if (client.readyState === client.OPEN) {
                        this.fixParser.setNextTargetMsgSeqNum(this.fixParser.getNextTargetMsgSeqNum() + 1);
                        client.send(encodedMessage);
                        this.messageBuffer.add(message.clone());
                        log('FIXServer (WEBSOCKET): >> sent', encodedMessage.replace(/\x01/g, '|'));
                    }
                });
            } else {
                logError('FIXServer (WEBSOCKET): could not send message, socket not connected', message);
            }
        }
    }

    public isConnected(): boolean {
        return this.connected;
    }

    private resetSession() {
        this.nextNumIn = 1;
    }

    public close(): void {
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket! as Socket;
            const server: Server = this.server! as Server;
            socket.end(() => {
                if (server) {
                    server.close(() => {
                        log(`FIXServer (${this.protocol.toUpperCase()}): ended session`);
                        this.initialize();
                    });
                }
            });
        } else if (this.protocol === 'websocket') {
            const server: Websocket.Server = this.server! as Websocket.Server;
            if (server) {
                server.clients.forEach((client: Websocket) => {
                    client.close();
                });
                server.close(() => {
                    log(`FIXServer (${this.protocol.toUpperCase()}): ended session`);
                    this.initialize();
                });
            }
        }
    }

    public stopHeartbeat(): void {
        clearInterval(this.heartBeatIntervalId!);
    }

    public startHeartbeat(heartBeatInterval: number = this.heartBeatInterval!): void {
        this.stopHeartbeat();
        log(`FIXServer: heartbeat configured to ${heartBeatInterval} seconds`);
        this.heartBeatInterval = heartBeatInterval;
        this.heartBeatIntervalId = setInterval(() => {
            const heartBeatMessage: Message = heartBeat(this);
            this.send(heartBeatMessage);
            const encodedMessage: string = heartBeatMessage.encode();
            log(`FIXServer (${this.protocol?.toUpperCase()}): >> Heartbeat sent`, encodedMessage.replace(/\x01/g, '|'));
        }, this.heartBeatInterval * 1000);
    }

    private processMessage(message: Message): void {
        log(`FIXServer (${this.protocol.toUpperCase()}): << received ${message.description}`);
        handleSequence(this, message);
        if (message.messageType === MessageEnum.SequenceReset) {
            handleSequenceReset(this, message);
        } else if (message.messageType === MessageEnum.TestRequest) {
            handleTestRequest(this, message);
        } else if (message.messageType === MessageEnum.Logon) {
            handleLogon(this, message);
        } else if (message.messageType === MessageEnum.Logout) {
            handleLogout(this, message);
        }
        this.nextNumIn++;
    }
}

export { EncryptMethodEnum as EncryptMethod } from './fieldtypes/EncryptMethodEnum';
export { ExecTypeEnum as ExecType } from './fieldtypes/ExecTypeEnum';
export { FieldEnum as Fields } from './fieldtypes/FieldEnum';
export { HandlInstEnum as HandlInst } from './fieldtypes/HandlInstEnum';
export { MarketDepthEnum as MarketDepth } from './fieldtypes/MarketDepthEnum';
export { MDUpdateTypeEnum as MDUpdateType } from './fieldtypes/MDUpdateTypeEnum';
export { MDEntryTypeEnum as MDEntryType } from './fieldtypes/MDEntryTypeEnum';
export { MessageEnum as Messages } from './fieldtypes/MessageEnum';
export { OrderTypesEnum as OrderTypes } from './fieldtypes/OrderTypesEnum';
export { OrderStatusEnum as OrderStatus } from './fieldtypes/OrderStatusEnum';
export { AllocPositionEffectEnum as AllocPositionEffect } from './fieldtypes/AllocPositionEffectEnum';
export { SideEnum as Side } from './fieldtypes/SideEnum';
export { SubscriptionRequestTypeEnum as SubscriptionRequestType } from './fieldtypes/SubscriptionRequestTypeEnum';
export { TimeInForceEnum as TimeInForce } from './fieldtypes/TimeInForceEnum';
export { Constants };
export { Field };
export { Message };
export { FIXServer };
