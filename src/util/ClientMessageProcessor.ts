/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2021 Victor Norgren
 * Released under the MIT license
 */
import { Message } from '../message/Message';
import { log, logWarning } from './util';
import { FIXParser } from '../FIXParser';
import { Field } from '../fields/Field';
import { FieldEnum } from '../fieldtypes/FieldEnum';
import { MessageEnum } from '../fieldtypes/MessageEnum';
import { FIXParserBrowser } from '../FIXParserBrowser';
import { heartBeat } from '../messagetemplates/MessageTemplates';

export const clientProcessMessage = (parser: FIXParser | FIXParserBrowser, message: Message): void => {
    if (message.messageSequence !== parser.nextNumIn) {
        logWarning(`FIXParser: expected MsgSeqNum ${parser.nextNumIn}, but got ${message.messageSequence}`);
    }

    if (message.messageType === MessageEnum.SequenceReset) {
        const newSeqNo = message.getField(FieldEnum.NewSeqNo)!.value;
        if (newSeqNo) {
            log(`FIXParser: new sequence number ${newSeqNo}`);
            parser.nextNumIn = Number(newSeqNo);
        }
    } else if (message.messageType === MessageEnum.TestRequest) {
        let heartBeatMessage: Message = heartBeat(parser);
        const testReqIdValue: string | null = message.getField(FieldEnum.TestReqID)
            ? message.getField(FieldEnum.TestReqID)!.value
            : null;
        if (testReqIdValue) {
            const testReqId: Field = new Field(FieldEnum.TestReqID, testReqIdValue);
            heartBeatMessage = heartBeat(parser, testReqId);
            parser.send(heartBeatMessage);
            log(`FIXParser: responded to TestRequest with Heartbeat<TestReqID=${testReqIdValue}>`);
        } else {
            parser.send(heartBeatMessage);
            log('FIXParser: >> responded to TestRequest with Heartbeat');
        }
    } else if (message.messageType === MessageEnum.Logon) {
        if (
            message.getField(FieldEnum.ResetSeqNumFlag) &&
            message.getField(FieldEnum.ResetSeqNumFlag)!.value.toString() === 'Y'
        ) {
            log('FIXParser: Logon contains ResetSeqNumFlag=Y, resetting sequence numbers to 1');
            parser.nextNumIn = 1;
        }

        const heartBeatInterval = message.getField(FieldEnum.HeartBtInt)
            ? message.getField(FieldEnum.HeartBtInt)!.value
            : parser.heartBeatInterval;
        parser.startHeartbeat(heartBeatInterval);
    }
    parser.nextNumIn++;
    log(`FIXParser (${parser.protocol?.toUpperCase()}): << received ${message.description}`);
};
