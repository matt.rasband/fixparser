/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2021 Victor Norgren
 * Released under the MIT license
 */
import { Message } from './../message/Message';

const MAX_BUFFER: number = 2500;

export class MessageBuffer {
    private buffer: Message[] = [];

    public add(message: Message): void {
        if (this.buffer.length === MAX_BUFFER) {
            this.buffer.pop();
        }
        this.buffer.unshift(message);
    }

    public get(): Message[] {
        return this.buffer;
    }
}
