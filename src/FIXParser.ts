/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2021 Victor Norgren
 * Released under the MIT license
 */
import { EventEmitter } from 'events';
import { Socket } from 'net';
import WebSocket from 'ws';

import { IFIXParser } from './IFIXParser';
import * as Constants from './fieldtypes';
import { Field } from './fields/Field';
import { FIXParserBase } from './FIXParserBase';
import { Message } from './message/Message';
import { version, DEFAULT_FIX_VERSION, log, logError, loggingSettings, timestamp, Version } from './util/util';
import { FrameDecoder } from './util/FrameDecoder';
import { heartBeat } from './messagetemplates/MessageTemplates';
import { URL } from 'url';
import { HttpsProxyAgent } from 'https-proxy-agent';
import tls, { TLSSocket } from 'tls';
import { clientProcessMessage } from './util/ClientMessageProcessor';

export type Protocol = 'tcp' | 'ssl-tcp' | 'websocket';

export type Options = {
    host?: string;
    port?: number;
    protocol?: Protocol;
    sender?: string;
    target?: string;
    heartbeatIntervalSeconds?: number;
    fixVersion?: string;
    sslKey?: Buffer | null;
    sslCert?: Buffer | null;
    logging?: boolean;
    proxy?: string | null;
};

export default class FIXParser extends EventEmitter implements IFIXParser {
    public static version: Version = version;

    public fixParserBase: FIXParserBase = new FIXParserBase();
    public nextNumIn: number = 1;
    public nextNumOut: number = 1;
    public heartBeatIntervalId: ReturnType<typeof setInterval> | null = null;
    public socket: Socket | WebSocket | TLSSocket | null = null;
    public connected: boolean = false;
    public host: string | null = null;
    public port: number | null = null;
    public protocol: Protocol | null = 'tcp';
    public sender: string | null = null;
    public target: string | null = null;
    public heartBeatInterval: number | undefined;
    public fixVersion: string = DEFAULT_FIX_VERSION;

    public connect({
        host = 'localhost',
        port = 9878,
        protocol = 'tcp',
        sender = 'SENDER',
        target = 'TARGET',
        heartbeatIntervalSeconds = 30,
        fixVersion = this.fixVersion,
        sslKey = null,
        sslCert = null,
        logging = true,
        proxy = null,
    }: Options = {}): void {
        this.fixVersion = fixVersion;
        this.fixParserBase.fixVersion = fixVersion;
        this.protocol = protocol;
        this.sender = sender;
        this.target = target;
        this.heartBeatInterval = heartbeatIntervalSeconds;
        loggingSettings.enabled = logging;

        if (protocol === 'tcp') {
            this.socket = new Socket();
            this.socket.setEncoding('ascii');
            this.socket.pipe(new FrameDecoder()).on('data', (data: any) => {
                const messages: Message[] = this.parse(data.toString());
                let i = 0;
                for (i; i < messages.length; i++) {
                    clientProcessMessage(this, messages[i]);
                    this.emit('message', messages[i]);
                }
            });
            this.socket.on('close', () => {
                this.connected = false;
                this.emit('close');
                this.stopHeartbeat();
            });
            this.socket.on('error', (error) => {
                this.connected = false;
                this.emit('error', error);
                this.stopHeartbeat();
            });
            this.socket.on('timeout', () => {
                this.connected = false;
                const socket: Socket = this.socket! as Socket;
                this.emit('timeout');
                socket.end();
                this.stopHeartbeat();
            });
            this.socket.connect(port, host, () => {
                this.connected = true;
                log('Connected');
                this.emit('open');
            });
        } else if (protocol === 'websocket') {
            const connectionString =
                host.indexOf('ws://') === -1 && host.indexOf('wss://') === -1
                    ? `ws://${host}:${port}`
                    : `${host}:${port}`;
            if (proxy) {
                const proxyUrl: URL = new URL(proxy);
                const agent: HttpsProxyAgent = new HttpsProxyAgent(proxyUrl);
                this.socket = new WebSocket(connectionString, { agent });
            } else {
                this.socket = new WebSocket(connectionString);
            }
            this.socket.on('open', () => {
                log('Connected');
                this.connected = true;
                this.emit('open');
            });
            this.socket.on('message', (data: string | Buffer) => {
                const messages = this.parse(data.toString());
                let i = 0;
                for (i; i < messages.length; i++) {
                    clientProcessMessage(this, messages[i]);
                    this.emit('message', messages[i]);
                }
            });
            this.socket.on('close', () => {
                this.connected = false;
                this.emit('close');
                this.stopHeartbeat();
            });
        } else if (protocol === 'ssl-tcp') {
            const options = {
                key: sslKey!,
                cert: sslCert!,
                rejectUnauthorized: false,
            };
            this.socket = tls.connect(port, host, options, () => {
                this.connected = true;
                this.emit('open');
                log('FIXParserSslClientSocket: connected through SSL');

                process.stdin.pipe(this.socket as TLSSocket);
                process.stdin.resume();
            });
            this.socket.setEncoding('utf8');
            this.socket.on('data', (data: any) => {
                const messages: Message[] = this.parse(data.toString());
                let i = 0;
                for (i; i < messages.length; i++) {
                    clientProcessMessage(this, messages[i]);
                    this.emit('message', messages[i]);
                }
            });
            this.socket.on('error', (error: any) => {
                this.connected = false;
                this.emit('error', error);
                this.stopHeartbeat();
            });
            this.socket.on('close', () => {
                this.connected = false;
                this.emit('close');
                this.stopHeartbeat();
            });
            this.socket.on('timeout', () => {
                const socket: TLSSocket = this.socket! as TLSSocket;
                this.connected = false;
                this.emit('timeout');
                socket.end();
                this.stopHeartbeat();
            });
        }
    }

    public getNextTargetMsgSeqNum(): number {
        return this.nextNumOut;
    }

    public setNextTargetMsgSeqNum(nextMsgSeqNum: number): number {
        this.nextNumOut = nextMsgSeqNum;
        return this.nextNumOut;
    }

    public getTimestamp(dateObject = new Date()): string {
        return timestamp(dateObject);
    }

    public createMessage(...fields: Field[]): Message {
        return new Message(this.fixVersion, ...fields);
    }

    public parse(data: string): Message[] {
        return this.fixParserBase.parse(data);
    }

    public send(message: Message): void {
        const encodedMessage: string = message.encode();
        if (this.protocol === 'tcp' && this.connected) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            (this.socket! as Socket).write(encodedMessage);
            log('FIXParser (TCP): >> sent', encodedMessage.replace(/\x01/g, '|'));
        } else if (this.protocol === 'websocket' && (this.socket! as WebSocket).readyState === WebSocket.OPEN) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            (this.socket! as WebSocket).send(encodedMessage);
            log('FIXParser (Websocket): >> sent', encodedMessage.replace(/\x01/g, '|'));
        } else if (this.protocol === 'ssl-tcp' && this.connected) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            (this.socket! as TLSSocket).write(encodedMessage);
            log('FIXParser (TLS-TCP): >> sent', encodedMessage.replace(/\x01/g, '|'));
        } else {
            logError('FIXParser: could not send message, no connection', encodedMessage.replace(/\x01/g, '|'));
        }
    }

    public isConnected(): boolean {
        return this.connected;
    }

    public close(): void {
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket! as Socket;
            if (socket) {
                socket.destroy();
                this.connected = false;
            } else {
                logError('FIXParser (TCP): could not close socket, connection not open');
            }
        } else if (this.protocol === 'websocket') {
            const socket: WebSocket = this.socket! as WebSocket;
            if (socket) {
                socket.close();
                this.connected = false;
            } else {
                logError('FIXParser (Websocket): could not close socket, connection not open');
            }
        } else if (this.protocol === 'ssl-tcp') {
            const socket: TLSSocket = this.socket! as TLSSocket;
            if (socket) {
                socket.destroy();
                this.connected = false;
            } else {
                logError('FIXParser (TLS-TCP): could not close socket, connection not open');
            }
        }
    }

    public stopHeartbeat(): void {
        clearInterval(this.heartBeatIntervalId!);
    }

    public startHeartbeat(heartBeatInterval: number = this.heartBeatInterval!): void {
        this.stopHeartbeat();
        log(`FIXParser Heartbeat configured to ${heartBeatInterval} seconds`);
        this.heartBeatInterval = heartBeatInterval;
        this.heartBeatIntervalId = setInterval(() => {
            const heartBeatMessage: Message = heartBeat(this);
            this.send(heartBeatMessage);
            const encodedMessage: string = heartBeatMessage.encode();
            log(`FIXParser (${this.protocol?.toUpperCase()}): >> Heartbeat sent`, encodedMessage.replace(/\x01/g, '|'));
        }, this.heartBeatInterval * 1000);
    }
}

export { EncryptMethodEnum as EncryptMethod } from './fieldtypes/EncryptMethodEnum';
export { ExecTypeEnum as ExecType } from './fieldtypes/ExecTypeEnum';
export { FieldEnum as Fields } from './fieldtypes/FieldEnum';
export { HandlInstEnum as HandlInst } from './fieldtypes/HandlInstEnum';
export { MarketDepthEnum as MarketDepth } from './fieldtypes/MarketDepthEnum';
export { MDUpdateTypeEnum as MDUpdateType } from './fieldtypes/MDUpdateTypeEnum';
export { MDEntryTypeEnum as MDEntryType } from './fieldtypes/MDEntryTypeEnum';
export { MessageEnum as Messages } from './fieldtypes/MessageEnum';
export { OrderTypesEnum as OrderTypes } from './fieldtypes/OrderTypesEnum';
export { OrderStatusEnum as OrderStatus } from './fieldtypes/OrderStatusEnum';
export { AllocPositionEffectEnum as AllocPositionEffect } from './fieldtypes/AllocPositionEffectEnum';
export { SideEnum as Side } from './fieldtypes/SideEnum';
export { SubscriptionRequestTypeEnum as SubscriptionRequestType } from './fieldtypes/SubscriptionRequestTypeEnum';
export { TimeInForceEnum as TimeInForce } from './fieldtypes/TimeInForceEnum';
export { Constants };
export { Field };
export { Message };
export { FIXParser };

/**
 * Export global to the window object.
 */
(global as any).FIXParser = FIXParser;
