/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2021 Victor Norgren
 * Released under the MIT license
 */
import FIXServer from '../FIXServer';
import { Message } from '../message/Message';
import { FieldEnum } from '../fieldtypes/FieldEnum';
import { log } from '../util/util';
import { Field } from '../fields/Field';
import { heartBeat } from '../messagetemplates/MessageTemplates';

export const handleTestRequest = (parser: FIXServer, message: Message): void => {
    let heartBeatMessage: Message = heartBeat(this);
    const testReqIdValue: string | null = message.getField(FieldEnum.TestReqID)
        ? message.getField(FieldEnum.TestReqID)!.value
        : null;
    if (testReqIdValue) {
        const testReqId: Field = new Field(FieldEnum.TestReqID, testReqIdValue);
        heartBeatMessage = heartBeat(this, testReqId);
        parser.send(heartBeatMessage);
        log(
            `FIXServer (${parser.protocol.toUpperCase()}): >> responded to TestRequest with Heartbeat<TestReqID=${testReqIdValue}>`,
        );
    } else {
        parser.send(heartBeatMessage);
        log('FIXServer (${parser.protocol.toUpperCase()}): >> responded to TestRequest with Heartbeat');
    }
};
