/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2021 Victor Norgren
 * Released under the MIT license
 */
import FIXServer from '../FIXServer';
import { Message } from '../message/Message';
import { FieldEnum } from '../fieldtypes/FieldEnum';
import { log } from '../util/util';

export const handleSequenceReset = (parser: FIXServer, message: Message): void => {
    const newSeqNo = message.getField(FieldEnum.NewSeqNo)!.value;
    if (newSeqNo) {
        log(`FIXServer (${parser.protocol.toUpperCase()}): new sequence number ${newSeqNo}`);
        parser.setNextTargetMsgSeqNum(newSeqNo);
    }
};
